# Introduction

If you're here you're most likely thinking about doing what I did with this project: Building your "Hello World" Emulator. If so hop over onto the [Emulator Developement Discord](https://discord.gg/S27ceeta) the folks over there are awesome and glad to help!

# Requirements

All requirements should be in the [flake](./flake.nix). I don't know which of those are present on a normal Linux System (or even windows) and which aren't sorry

# Usage

Build with `cargo buil --release` then the binary is at `target/debug/ruschip`
