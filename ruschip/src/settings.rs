use std::{fs, path::PathBuf};

use iced::{
    widget::{column, container, row, text, text_input},
    Color, Command, Element,
};
use serde::{Deserialize, Serialize};

use crate::Message;

/// A setting has been changed
#[derive(Clone, Debug)]
#[allow(clippy::module_name_repetitions)]
pub enum SettingsMessage {
    Ipf(String),
    OnColor(ColorMessage),
    OffColor(ColorMessage),
    Pitch(String),
}

/// A color has been changed
#[derive(Clone, Debug)]
pub enum ColorMessage {
    R(String),
    G(String),
    B(String),
}

/// The actual application settings
#[derive(Serialize, Deserialize)]
pub struct Settings {
    #[serde(default = "Settings::default_schip_ips")]
    pub schip_ips: u32,
    #[serde(default = "Settings::default_off_color")]
    pub off_color: [u8; 3],
    #[serde(default = "Settings::default_on_color")]
    pub on_color: [u8; 3],
    #[serde(default = "Settings::default_pitch")]
    pub pitch: f32,
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            schip_ips: Self::default_schip_ips(),
            off_color: Self::default_off_color(),
            on_color: Self::default_on_color(),
            pitch: Self::default_pitch(),
        }
    }
}

impl Settings {
    fn default_schip_ips() -> u32 {
        30
    }

    fn default_off_color() -> [u8; 3] {
        let color = &Color::BLACK.into_rgba8();
        [color[0], color[1], color[2]]
    }

    fn default_on_color() -> [u8; 3] {
        let color = Color::from_rgb8(0x8A, 0, 0).into_rgba8();
        [color[0], color[1], color[2]]
    }

    fn default_pitch() -> f32 {
        523.25
    }

    fn file_path() -> PathBuf {
        let mut path = dirs::config_dir().expect("Couldn't get a config dir");
        path.push("ruschip.toml");
        path
    }

    fn load() -> Self {
        let path = Self::file_path();
        if path.exists() {
            let settings = fs::read_to_string(path).expect("Couldn't read the settings file");
            toml::from_str(&settings).expect("Couldn't parse settings")
        } else {
            Settings::default()
        }
    }

    fn save(&self) {
        let path = Self::file_path();
        fs::write(
            path,
            toml::to_string(self).expect("Couldn't serialize settings"),
        )
        .expect("Couldn't save settings");
    }
}

/// Shows the settings window
pub struct SettingsWindow {
    pub show: bool,
    ipf_string: String,
    off_color_string: [String; 3],
    on_color_string: [String; 3],
    pitch_string: String,
    pub settings: Settings,
}

impl SettingsWindow {
    pub fn new() -> Self {
        let settings = Settings::load();
        let off_color_string = [
            settings.off_color[0].to_string(),
            settings.off_color[1].to_string(),
            settings.off_color[2].to_string(),
        ];
        let on_color_string = [
            settings.on_color[0].to_string(),
            settings.on_color[1].to_string(),
            settings.on_color[2].to_string(),
        ];
        Self {
            show: Default::default(),
            ipf_string: settings.schip_ips.to_string(),
            off_color_string,
            on_color_string,
            pitch_string: settings.pitch.to_string(),
            settings,
        }
    }

    pub fn view(&self) -> Element<'_, Message> {
        container(column!(
            text("Settings").size(24),
            row!(
                text("Instructions per Frame"),
                text_input("Instructions per Frame", &self.ipf_string)
                    .on_input(|value| { Message::Settings(SettingsMessage::Ipf(value)) })
                    .width(120)
            )
            .spacing(10),
            row!(
                text("Off Collor"),
                text_input("R", &self.off_color_string[0])
                    .on_input(|value| {
                        Message::Settings(SettingsMessage::OffColor(ColorMessage::R(value)))
                    })
                    .width(120),
                text_input("G", &self.off_color_string[1])
                    .on_input(|value| {
                        Message::Settings(SettingsMessage::OffColor(ColorMessage::G(value)))
                    })
                    .width(120),
                text_input("B", &self.off_color_string[2])
                    .on_input(|value| {
                        Message::Settings(SettingsMessage::OffColor(ColorMessage::B(value)))
                    })
                    .width(120)
            )
            .spacing(10),
            row!(
                text("On Collor"),
                text_input("R", &self.on_color_string[0])
                    .on_input(|value| {
                        Message::Settings(SettingsMessage::OnColor(ColorMessage::R(value)))
                    })
                    .width(120),
                text_input("G", &self.on_color_string[1])
                    .on_input(|value| {
                        Message::Settings(SettingsMessage::OnColor(ColorMessage::G(value)))
                    })
                    .width(120),
                text_input("B", &self.on_color_string[2])
                    .on_input(|value| {
                        Message::Settings(SettingsMessage::OnColor(ColorMessage::B(value)))
                    })
                    .width(120)
            )
            .spacing(10),
            row!(
                text("Beep Pitcj"),
                text_input("Beep Pitcj", &self.pitch_string)
                    .on_input(|value| { Message::Settings(SettingsMessage::Pitch(value)) })
                    .width(120)
            )
            .spacing(10),
        ))
        .into()
    }

    pub fn update(&mut self, message: SettingsMessage) -> Command<Message> {
        match message {
            SettingsMessage::Ipf(ips_string) => {
                if let Ok(ips) = ips_string.parse() {
                    self.settings.schip_ips = ips;
                    self.ipf_string = ips_string;
                    self.settings.save();
                }
            }
            SettingsMessage::OnColor(color) => match color {
                ColorMessage::R(color_string) => {
                    if let Ok(color) = color_string.parse() {
                        self.settings.on_color[0] = color;
                        self.on_color_string[0] = color_string;
                        self.settings.save();
                    }
                }
                ColorMessage::G(color_string) => {
                    if let Ok(color) = color_string.parse() {
                        self.settings.on_color[1] = color;
                        self.on_color_string[1] = color_string;
                        self.settings.save();
                    }
                }
                ColorMessage::B(color_string) => {
                    if let Ok(color) = color_string.parse() {
                        self.settings.on_color[2] = color;
                        self.on_color_string[2] = color_string;
                        self.settings.save();
                    }
                }
            },
            SettingsMessage::OffColor(color) => match color {
                ColorMessage::R(color_string) => {
                    if let Ok(color) = color_string.parse() {
                        self.settings.off_color[0] = color;
                        self.off_color_string[0] = color_string;
                        self.settings.save();
                    }
                }
                ColorMessage::G(color_string) => {
                    if let Ok(color) = color_string.parse() {
                        self.settings.off_color[1] = color;
                        self.off_color_string[1] = color_string;
                        self.settings.save();
                    }
                }
                ColorMessage::B(color_string) => {
                    if let Ok(color) = color_string.parse() {
                        self.settings.off_color[2] = color;
                        self.off_color_string[2] = color_string;
                        self.settings.save();
                    }
                }
            },
            SettingsMessage::Pitch(pitch_string) => {
                if let Ok(pitch) = pitch_string.parse() {
                    self.settings.pitch = pitch;
                    self.pitch_string = pitch_string;
                    self.settings.save();
                }
            }
        }
        Command::none()
    }
}
