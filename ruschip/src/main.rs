use std::{
    collections::HashMap,
    fs::File,
    io::{Read, Write},
    sync::Arc,
    time::Duration,
};

use chip8lib::Chip8;
use display::Display;
use iced::{
    advanced::graphics::futures::subscription,
    alignment::{Horizontal, Vertical},
    event, executor,
    futures::SinkExt,
    keyboard,
    multi_window::Application,
    widget::{button, column, container, text, Canvas},
    window, Color, Command, Element, Event, Length, Subscription, Theme,
};
use iced_aw::{
    menu::{Item, Menu},
    menu_bar, menu_items,
};
use inspectors::memory::MemoryMessage;
use rfd::{AsyncFileDialog, AsyncMessageDialog, MessageButtons, MessageLevel};
use rodio::{source::SineWave, OutputStream, Sink, Source};
use settings::{SettingsMessage, SettingsWindow};

mod display;
mod inspectors;
mod settings;

fn main() -> iced::Result {
    env_logger::init();
    App::run(iced::Settings::default())
}

pub enum Windows {
    Settings,
    Memory,
    Register,
    Stack,
    Timers,
}

#[derive(Debug, Clone)]
pub enum Message {
    Noop,
    ChooseFile,
    Open(Vec<u8>),
    ClosedWindow(window::Id),
    Tick,
    ShowSettings,
    ShowMemory,
    ShowRegisters,
    ShowStack,
    ShowTimers,
    Settings(SettingsMessage),
    Memory(MemoryMessage),
    KeyPressed(usize),
    KeyReleased(usize),
    Error(Arc<dyn std::error::Error + Send + Sync>),
}

struct App {
    chip8: Chip8,
    settings: SettingsWindow,
    memory: inspectors::Memory,
    registers: inspectors::Registers,
    stack: inspectors::Stack,
    timers: inspectors::Timers,
    running: bool,
    windows: HashMap<window::Id, Windows>,
    key: [bool; 16],
    sink: Option<Sink>,
    #[allow(dead_code)]
    output_stream: OutputStream,
    main_message: (String, Color),
}

impl Application for App {
    type Message = Message;

    type Executor = executor::Default;

    type Theme = Theme;

    type Flags = ();

    fn new((): Self::Flags) -> (App, Command<Message>) {
        let (output_stream, stream_handle) =
            OutputStream::try_default().expect("Couldn't get an output stream");
        let sink = Sink::try_new(&stream_handle).ok();
        sink.as_ref().inspect(|sink| {
            sink.pause();
        });

        let save_file = {
            let mut path = dirs::data_dir().expect("There should always be a data dir");
            path.push("rusty_chip.sc8.sv");
            if !path.exists() {
                File::create(&path)
                    .expect("Can't create the save file")
                    .write_all(&[0; 16])
                    .expect("Can't write to the save file");
            }
            path
        };
        let save_flags = {
            let save_file = save_file.clone();
            Box::new(move |flags: &[u8]| {
                let mut content = [0; 16];
                content[..flags.len()].copy_from_slice(flags);
                File::create(&save_file)?.write_all(&content)?;
                Ok(())
            })
        };
        let load_flags = Box::new(move || {
            let mut buf = Vec::with_capacity(16);
            File::open(&save_file)?.read_to_end(&mut buf)?;
            Ok(buf)
        });

        (
            Self {
                chip8: Chip8::new(save_flags, load_flags),
                settings: SettingsWindow::new(),
                memory: inspectors::Memory::default(),
                registers: inspectors::Registers::default(),
                stack: inspectors::Stack::default(),
                timers: inspectors::Timers::default(),
                running: false,
                windows: HashMap::with_capacity(5),
                key: [false; 16],
                output_stream,
                sink,
                main_message: (String::from("Load a Rom!"), Color::BLACK),
            },
            Command::none(),
        )
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        let subscriptions = [
            render(),
            event::listen_with(|event, _| match event {
                Event::Window(id, event) => {
                    if window::Event::Closed == event {
                        Some(Message::ClosedWindow(id))
                    } else {
                        None
                    }
                }

                Event::Keyboard(event) => match event {
                    keyboard::Event::KeyPressed { key, .. } => match key {
                        keyboard::Key::Character(character) => match character.as_str() {
                            "1" => Some(Message::KeyPressed(1)),
                            "2" => Some(Message::KeyPressed(2)),
                            "3" => Some(Message::KeyPressed(3)),
                            "4" => Some(Message::KeyPressed(0xC)),
                            "q" => Some(Message::KeyPressed(4)),
                            "w" => Some(Message::KeyPressed(5)),
                            "e" => Some(Message::KeyPressed(6)),
                            "r" => Some(Message::KeyPressed(0xD)),
                            "a" => Some(Message::KeyPressed(7)),
                            "s" => Some(Message::KeyPressed(8)),
                            "d" => Some(Message::KeyPressed(9)),
                            "f" => Some(Message::KeyPressed(0xE)),
                            "y" => Some(Message::KeyPressed(0xA)),
                            "x" => Some(Message::KeyPressed(0)),
                            "c" => Some(Message::KeyPressed(0xB)),
                            "v" => Some(Message::KeyPressed(0xF)),
                            _ => None,
                        },
                        _ => None,
                    },
                    keyboard::Event::KeyReleased { key, .. } => match key {
                        keyboard::Key::Character(character) => match character.as_str() {
                            "1" => Some(Message::KeyReleased(1)),
                            "2" => Some(Message::KeyReleased(2)),
                            "3" => Some(Message::KeyReleased(3)),
                            "4" => Some(Message::KeyReleased(0xC)),
                            "q" => Some(Message::KeyReleased(4)),
                            "w" => Some(Message::KeyReleased(5)),
                            "e" => Some(Message::KeyReleased(6)),
                            "r" => Some(Message::KeyReleased(0xD)),
                            "a" => Some(Message::KeyReleased(7)),
                            "s" => Some(Message::KeyReleased(8)),
                            "d" => Some(Message::KeyReleased(9)),
                            "f" => Some(Message::KeyReleased(0xE)),
                            "y" => Some(Message::KeyReleased(0xA)),
                            "x" => Some(Message::KeyReleased(0)),
                            "c" => Some(Message::KeyReleased(0xB)),
                            "v" => Some(Message::KeyReleased(0xF)),
                            _ => None,
                        },
                        _ => None,
                    },

                    keyboard::Event::ModifiersChanged(_) => None,
                },
                _ => None,
            }),
        ];
        Subscription::batch(subscriptions)
    }

    fn title(&self, window: window::Id) -> String {
        match window {
            window::Id::MAIN => String::from("Chip8"),
            _ => match self.windows.get(&window).expect("should always exist") {
                Windows::Settings => String::from("Chip8 Settings"),
                Windows::Memory => String::from("Chip8 Memory"),
                Windows::Register => String::from("Chip8 Registers"),
                Windows::Stack => String::from("Chip8 Stack"),
                Windows::Timers => String::from("Chip8 Timers"),
            },
        }
    }

    fn update(&mut self, message: Self::Message) -> Command<Message> {
        match message {
            Message::Noop => Command::none(),
            Message::ChooseFile => Command::perform(
                async {
                    let file = AsyncFileDialog::new()
                        .add_filter("Chip8 Rom", &["ch8", "sc8"])
                        .set_directory("/")
                        .pick_file()
                        .await;
                    if let Some(file) = file {
                        Some(file.read().await)
                    } else {
                        None
                    }
                },
                |file| file.map_or(Message::Noop, Message::Open),
            ),
            Message::Open(file) => {
                if let Err(e) = self.chip8.load_and_start(&file) {
                    let e = Arc::new(e);
                    return Command::perform(async {}, |_| Message::Error(e));
                }
                self.running = true;
                self.main_message.0.clear();
                Command::none()
            }
            Message::ShowSettings => {
                if self.settings.show {
                    Command::none()
                } else {
                    self.settings.show = true;
                    let (id, spawn_window) = window::spawn(window::Settings::default());
                    self.windows.insert(id, Windows::Settings);
                    spawn_window
                }
            }
            Message::ShowMemory => {
                if self.memory.show {
                    Command::none()
                } else {
                    self.memory.show = true;
                    let (id, spawn_window) = window::spawn(window::Settings::default());
                    self.windows.insert(id, Windows::Memory);
                    spawn_window
                }
            }
            Message::ShowRegisters => {
                if self.registers.show {
                    Command::none()
                } else {
                    self.registers.show = true;
                    let (id, spawn_window) = window::spawn(window::Settings::default());
                    self.windows.insert(id, Windows::Register);
                    spawn_window
                }
            }
            Message::ShowStack => {
                if self.stack.show {
                    Command::none()
                } else {
                    self.stack.show = true;
                    let (id, spawn_window) = window::spawn(window::Settings::default());
                    self.windows.insert(id, Windows::Stack);
                    spawn_window
                }
            }
            Message::ShowTimers => {
                if self.timers.show {
                    Command::none()
                } else {
                    self.timers.show = true;
                    let (id, spawn_window) = window::spawn(window::Settings::default());
                    self.windows.insert(id, Windows::Timers);
                    spawn_window
                }
            }
            Message::Tick => self.tick(),
            Message::ClosedWindow(id) => {
                if id == window::Id::MAIN {
                    return Command::batch(self.windows.keys().map(|a| window::close(*a)));
                }
                let window_type = self.windows.remove(&id).expect("Should always be there");
                match window_type {
                    Windows::Settings => self.settings.show = false,
                    Windows::Memory => self.memory.show = false,
                    Windows::Register => self.registers.show = false,
                    Windows::Stack => self.stack.show = false,
                    Windows::Timers => self.timers.show = false,
                }
                Command::none()
            }
            Message::Settings(message) => self.settings.update(message),
            Message::Memory(message) => self.memory.update(message),
            Message::KeyPressed(key) => {
                self.key[key] = true;
                Command::none()
            }
            Message::KeyReleased(key) => {
                self.key[key] = false;
                Command::none()
            }
            Message::Error(error) => {
                self.running = false;
                Command::perform(
                    async move {
                        AsyncMessageDialog::new()
                            .set_level(MessageLevel::Error)
                            .set_title("An Error has occured")
                            .set_description(error.to_string())
                            .set_buttons(MessageButtons::Ok)
                            .show()
                            .await;
                    },
                    |_| Message::Noop,
                )
            }
        }
    }

    fn view(&self, window: window::Id) -> Element<'_, Self::Message> {
        match window {
            window::Id::MAIN => {
                let off_color = self.settings.settings.off_color;
                let on_color = self.settings.settings.on_color;
                let off_color = Color::from_rgb8(off_color[0], off_color[1], off_color[2]);
                let on_color = Color::from_rgb8(on_color[0], on_color[1], on_color[2]);

                container(column!(
                    menu_bar!((
                        button(text("File")).on_press(Message::Noop),
                        Menu::new(menu_items!((button(text("Open"))
                            .on_press(Message::ChooseFile)
                            .width(Length::Fill))(
                            button(text("Setings"))
                                .on_press(Message::ShowSettings)
                                .width(Length::Fill)
                        )))
                        .max_width(180.0)
                        .offset(0.0)
                        .spacing(5.0)
                    )(
                        button(text("Inspectors")).on_press(Message::Noop),
                        Menu::new(menu_items!((button(text("Memory"))
                            .on_press(Message::ShowMemory)
                            .width(Length::Fill))(
                            button(text("Registers"))
                                .on_press(Message::ShowRegisters)
                                .width(Length::Fill)
                        )(
                            button(text("Stack"))
                                .on_press(Message::ShowStack)
                                .width(Length::Fill)
                        )(
                            button(text("Timers"))
                                .on_press(Message::ShowTimers)
                                .width(Length::Fill)
                        )))
                        .max_width(180.0)
                        .offset(0.0)
                        .spacing(5.0)
                    )),
                    text(&self.main_message.0).style(self.main_message.1),
                    container(
                        Canvas::new(Display::new(
                            self.chip8.display().clone(),
                            off_color,
                            on_color
                        ))
                        .height(Length::Fill)
                        .width(Length::Fill)
                    )
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .align_x(Horizontal::Center)
                    .align_y(Vertical::Center)
                ))
                .width(Length::Fill)
                .height(Length::Fill)
                .into()
            }

            _ => match self.windows.get(&window).expect("Should always be there") {
                Windows::Settings => self.settings.view(),
                Windows::Memory => self.memory.view(self.chip8.memory()),
                Windows::Register => inspectors::Registers::view(self.chip8.registers()),
                Windows::Stack => inspectors::Stack::view(self.chip8.stack()),
                Windows::Timers => inspectors::Timers::view(&self.chip8.timers),
            },
        }
    }
}

impl App {
    fn tick(&mut self) -> Command<Message> {
        self.chip8.new_keys(self.key);
        self.chip8.timers.decrement();
        if self.running {
            for _ in 0..self.settings.settings.schip_ips {
                match self.chip8.step() {
                    Ok(finished) => {
                        if finished {
                            self.running = false;
                            self.main_message =
                                (String::from("Rom Execution Finished"), Color::BLACK);
                        }
                    }
                    Err(err) => {
                        let err = Arc::new(err);
                        return Command::perform(async {}, |_| Message::Error(err));
                    }
                }
            }
        }
        if self.chip8.timers.sound_timer > 0 {
            self.sink.as_ref().inspect(|sink| {
                if sink.is_paused() {
                    let source = SineWave::new(self.settings.settings.pitch).amplify(0.25);
                    sink.append(source);
                    sink.play();
                }
            });
        } else {
            self.sink.as_ref().inspect(|sink| sink.clear());
        }
        Command::none()
    }
}

fn render() -> Subscription<Message> {
    struct Render;

    subscription::channel(
        std::any::TypeId::of::<Render>(),
        100,
        |mut output| async move {
            let sleep_time = Duration::from_secs(1) / 60;
            loop {
                output
                    .send(Message::Tick)
                    .await
                    .expect("Should always be able to send");
                tokio::time::sleep(sleep_time).await;
            }
        },
    )
}
