use iced::{
    widget::canvas::{self, Frame, Program},
    Color, Point, Renderer, Size, Theme,
};

use crate::Message;

/// The Widget showing a chip8 display
#[derive(Clone)]
pub struct Display(chip8lib::Display, Color, Color);

impl Display {
    pub fn new(display: chip8lib::Display, off_color: Color, on_color: Color) -> Self {
        Self(display, off_color, on_color)
    }
}

impl Program<Message> for Display {
    type State = ();

    fn draw(
        &self,
        _state: &Self::State,
        renderer: &Renderer,
        _theme: &Theme,
        bounds: iced::Rectangle,
        _cursor: iced::advanced::mouse::Cursor,
    ) -> Vec<<Renderer as canvas::Renderer>::Geometry> {
        let mut frame = Frame::new(renderer, bounds.size());

        let height = f32::from(u16::try_from(self.0.len()).expect("Should always fit in an u16"));
        let width = f32::from(
            u16::try_from(
                self.0
                    .get(0)
                    .expect("There is always at least one row")
                    .len(),
            )
            .expect("Should always fit in an u16"),
        );

        // Check how big the squares can be
        let size = (frame.size().height / height).min(frame.size().width / width);

        // Center the canvas
        let start_x = (frame.width() - width * size) / 2.0;
        let start_y = (frame.height() - height * size) / 2.0;

        // Draw the display
        for y in 0..self.0.len() {
            let row = &self.0.get(y).unwrap();
            row.iter().enumerate().for_each(|(x, pixel)| {
                let color = if *pixel { self.2 } else { self.1 };
                frame.fill_rectangle(
                    Point::new(
                        start_x
                            + f32::from(u16::try_from(x).expect("Should always fit in an u16"))
                                * size,
                        start_y
                            + f32::from(u16::try_from(y).expect("Should always fit in an u16"))
                                * size,
                    ),
                    Size::new(size, size),
                    color,
                );
            });
        }
        vec![frame.into_geometry()]
    }
}
