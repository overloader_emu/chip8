pub mod memory;

use iced::{
    alignment::Horizontal,
    widget::{column, container, row, text, Column, Space},
    Element, Font,
};
pub use memory::Memory;

use crate::Message;

/// A window to inspect registers
#[derive(Default)]
pub struct Registers {
    pub show: bool,
}

impl Registers {
    pub fn view(registers: &chip8lib::Registers) -> Element<'_, Message> {
        container(column!(
            text("Registers").size(24),
            row!(
                text("PC:").width(30),
                text(format!("{:03X}", registers.pc))
                    .width(30)
                    .font(Font::MONOSPACE)
                    .horizontal_alignment(Horizontal::Right),
                Space::new(20, 0),
                text("IX:").width(30),
                text(format!("{:03X}", registers.index))
                    .width(30)
                    .font(Font::MONOSPACE)
                    .horizontal_alignment(Horizontal::Right),
            ),
            Column::from_vec({
                let half = registers.v.len() / 2;
                let (first, second) = registers.v.split_at(half);
                first
                    .iter()
                    .zip(second.iter())
                    .enumerate()
                    .map(|(index, (a, b))| {
                        row!(
                            text(format!("V{:X}:", index)).width(30),
                            text(format!("{a:02X}"))
                                .width(30)
                                .font(Font::MONOSPACE)
                                .horizontal_alignment(Horizontal::Right),
                            Space::new(20, 0),
                            text(format!("V{:X}:", index + half)).width(30),
                            text(format!("{b:02X}"))
                                .width(30)
                                .font(Font::MONOSPACE)
                                .horizontal_alignment(Horizontal::Right),
                        )
                        .into()
                    })
                    .collect()
            })
        ))
        .into()
    }
}

#[derive(Default)]
pub struct Stack {
    pub show: bool,
}

impl Stack {
    pub fn view(stack: &chip8lib::Stack) -> Element<'_, Message> {
        container(column!(
            text("Stack").size(24),
            Column::from_vec(
                stack
                    .iter()
                    .map(|value| text(format!("{value:02X}")).into())
                    .collect()
            )
        ))
        .into()
    }
}

/// A window to show the current timer values
#[derive(Default)]
pub struct Timers {
    pub show: bool,
}

impl Timers {
    pub fn view(timers: &chip8lib::Timers) -> Element<'_, Message> {
        container(column!(
            text("Timers").size(24),
            row!(text(format!("Delay Timer: {}", timers.delay_timer))),
            row!(text(format!("Sound Timer: {}", timers.sound_timer)))
        ))
        .into()
    }
}
