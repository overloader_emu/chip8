use iced::{
    widget::{column, container, row, scrollable, text, text_input, Column, Row, Space},
    Color, Command, Element, Font, Padding,
};

use crate::Message;

/// A message that gets sent to update start and end addresses
#[derive(Debug, Clone)]
#[allow(clippy::module_name_repetitions)]
pub enum MemoryMessage {
    StartAddress(String),
    EndAddress(String),
}

/// A window to inspect memory
pub struct Memory {
    pub show: bool,
    start_address: usize,
    end_address: usize,
}

impl Default for Memory {
    fn default() -> Self {
        Self {
            show: Default::default(),
            start_address: 0,
            end_address: 0xFFF,
        }
    }
}

impl Memory {
    pub fn view(&self, mem: &chip8lib::Memory) -> Element<'_, Message> {
        let start_address = if self.start_address <= self.end_address {
            self.start_address
        } else {
            0
        };
        let end_address = if self.end_address < 0x1000 {
            self.end_address
        } else {
            0xFFF
        };

        container(
            column!(
                text("Memory").size(24),
                row!(
                    text("Start Address (hex):"),
                    text_input("Start Address", &format!("{:X}", self.start_address))
                        .width(100)
                        .on_input(|input| Message::Memory(MemoryMessage::StartAddress(input))),
                    Space::new(10, 0),
                    text("End Address (hex):"),
                    text_input("End Address", &format!("{:X}", self.end_address))
                        .width(100)
                        .on_input(|input| Message::Memory(MemoryMessage::EndAddress(input))),
                    Space::new(30, 0),
                    text(if self.start_address > self.end_address {
                        "Start Address may not be larger than End Address"
                    } else if self.end_address >= 0x1000 {
                        "End Address may not be larger than FFF"
                    } else {
                        ""
                    })
                    .style(Color::from_rgb8(0x8A, 0, 0))
                ),
                scrollable(Column::from_vec(
                    mem[start_address..=end_address]
                        .chunks(16)
                        .enumerate()
                        .map(|(index, chunk)| {
                            let mut row = vec![
                                text(format!("{:03X}:", start_address + index * 16))
                                    .font(Font::MONOSPACE)
                                    .into(),
                                Space::new(10, 0).into(),
                            ];
                            row.extend(chunk.iter().map(|mem| {
                                text(format!("{mem:02X}")).font(Font::MONOSPACE).into()
                            }));
                            Row::from_vec(row)
                                .spacing(10)
                                .padding(Padding::from([0, 10, 0, 0]))
                                .into()
                        })
                        .collect(),
                ))
            )
            .spacing(10),
        )
        .into()
    }

    pub fn update(&mut self, message: MemoryMessage) -> Command<Message> {
        match message {
            MemoryMessage::StartAddress(input) => {
                if let Ok(start_address) = usize::from_str_radix(&input, 16) {
                    self.start_address = start_address;
                }
            }
            MemoryMessage::EndAddress(input) => {
                if let Ok(end_address) = usize::from_str_radix(&input, 16) {
                    self.end_address = end_address;
                }
            }
        }
        Command::none()
    }
}
