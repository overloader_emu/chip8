use std::slice;

use thiserror::Error as ThisError;

/// The stack for function calls
#[derive(Default, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Stack {
    stack: [u16; 16],
    stack_pointer: usize,
}

impl Stack {
    /// Pushes an address on the stack
    ///
    /// # Errors
    ///
    /// Returns [`Error::Full`] if the stack is full
    pub fn push(&mut self, value: u16) -> Result<(), Error> {
        if let Some(cell) = self.stack.get_mut(self.stack_pointer) {
            *cell = value;
            self.stack_pointer += 1;
            Ok(())
        } else {
            log::error!("Tried pushing while the stack is full");
            Err(Error::Full(self.stack_pointer))
        }
    }

    /// Pops a value from the stack
    ///
    /// # Errors
    ///
    /// Returns [`Error::Empty`] if the stack is empty
    pub(crate) fn pop(&mut self) -> Result<u16, Error> {
        self.stack_pointer -= 1;
        self.stack
            .get(self.stack_pointer)
            .ok_or(Error::Empty(self.stack_pointer))
            .inspect_err(|_| log::error!("Tried popping while the stack is empty"))
            .copied()
    }

    pub fn iter(&self) -> slice::Iter<'_, u16> {
        self.stack[..self.stack_pointer].iter()
    }
}

impl<'a> IntoIterator for &'a Stack {
    type Item = &'a u16;

    type IntoIter = slice::Iter<'a, u16>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl IntoIterator for Stack {
    type Item = u16;

    type IntoIter = std::iter::Take<std::array::IntoIter<Self::Item, 16>>;

    fn into_iter(self) -> Self::IntoIter {
        self.stack.into_iter().take(self.stack_pointer)
    }
}

/// Is caused by some stack operation failing
#[derive(ThisError, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Error {
    /// Tried pushing onto the stack while it's full
    #[error("Tried pushing while the stack is full. Index is at {0}")]
    Full(usize),
    /// Tried popping from the stack while it's empty
    #[error("Tried popping while the stack is empty. Index is at {0}")]
    Empty(usize),
}
