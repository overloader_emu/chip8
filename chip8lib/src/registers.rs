use crate::opcodes;

/// The chip8 registers
#[derive(Default, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Registers {
    /// The Program Counter
    pub pc: u16,
    /// The Index register
    pub index: u16,
    /// The 16 V registers
    pub v: [u8; 16],
}

impl Registers {
    pub(crate) fn clear(&mut self) {
        self.pc = 200;
        self.index = 0;
        self.v = [0; 16];
    }

    /// Get a v register
    ///
    /// # Errors
    ///
    /// If the index is too large, return a [`opcodes::ExecutionError::OutOfBounds`]
    pub(crate) fn get_v(&self, index: usize) -> Result<u8, opcodes::ExecutionError> {
        self.v
            .get(index)
            .ok_or(opcodes::ExecutionError::OutOfBounds {
                structure: String::from("Registers"),
                length: self.v.len(),
                index,
            })
            .inspect_err(|e| log::error!("{e}"))
            .copied()
    }

    /// Get a v register mutably
    ///
    /// # Errors
    ///
    /// If the index is too large, return a [`opcodes::ExecutionError::OutOfBounds`]
    pub(crate) fn get_v_mut(&mut self, index: usize) -> Result<&mut u8, opcodes::ExecutionError> {
        let length = self.v.len();
        self.v
            .get_mut(index)
            .ok_or(opcodes::ExecutionError::OutOfBounds {
                structure: String::from("Registers"),
                length,
                index,
            })
            .inspect_err(|e| log::error!("{e}"))
    }
}
