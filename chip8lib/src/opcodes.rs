use std::iter;

use bitvec::{order::Msb0, view::BitView};
use log::trace;
use rand::random;
use thiserror::Error as ThisError;

use crate::{memory, stack, Chip8, Display};

/// An executable Opcode #[derive(Debug, Clone, Copy)]
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum Opcode {
    /// `00CN`
    ///
    /// Scrolls down by N lines
    ScrollDown(u8),

    /// `00E0`
    ///
    /// Clears the screen
    ClearScreen,

    /// `00EE`
    ///
    /// Return from subroutine
    Return,

    /// `00FB`
    ///
    /// Scrolls right by 4 columns
    ScrollRight,

    /// `00FC`
    ///
    /// Scrolls left by 4 columns
    ScrollLeft,

    /// `00FD`
    ///
    /// Exits the current running program
    Exit,

    /// `00FE`
    ///
    /// Switch screen to Low Res mode
    LowRes,

    /// `00FF`
    ///
    ///Switch screen to High Res mode
    HighRes,

    /// `1NNN`
    ///
    /// Jumps to the address NNN
    Jump(u16),

    /// `2NNN`
    ///
    /// Pushes the current pc to the stack and jumps to NNN
    Call(u16),

    /// `3XNN`
    ///
    /// Skips the next instruction if the value in VX is equal to NN
    SkipEquals(u8, u8),

    /// `4XNN`
    ///
    /// Skips the next instruction if the value in VX is not equal to NN
    SkipNotEquals(u8, u8),

    /// `5XY0`
    ///
    /// Skips the next instruction if the value in VX is equal to the value in VY
    SkipVEquals(u8, u8),

    /// `6XNN`
    ///
    /// Sets the value in VX to NN
    SetV(u8, u8),

    /// `7XNN`
    ///
    /// Adds NN to VX without putting the Overflow in VF
    AddValue(u8, u8),

    /// `8XY0`
    ///
    /// Set the value of VX to the value of VY
    SetVtoV(u8, u8),

    /// `8XY1`
    ///
    /// Set VX to the binary or of VX and VY
    Or(u8, u8),

    /// `8XY2`
    ///
    /// Set VX to the binary and of VX and VY
    And(u8, u8),

    /// `8XY3`
    ///
    /// Set VX to the binary xor of VX and VY
    Xor(u8, u8),

    /// `8XY4`
    ///
    /// Set VX to VX plus VY
    ///
    /// If an overflow occurs, set VF to 1, else to 0
    AddRegister(u8, u8),

    /// `8XY5`
    ///
    /// Set VX to VX minus VY
    ///
    /// If an underflow occurs, set VF to 0, else to 1
    SubtractX(u8, u8),

    /// `8XY6`
    ///
    /// Shift VX one bit to the right.
    /// Ignore VY
    ///
    /// If an underflow occurs, set VF to 1, else to 0
    ShiftRight(u8),

    /// `8XY7`
    ///
    /// Set VX to VY minus VX
    ///
    /// If an underflow occurs, set VF to 0, else to 1
    SubtractY(u8, u8),

    /// `8XYE`
    ///
    /// Shift VX one bit to the left.
    /// Ignore VY
    ///
    /// If an overflow occurs, set VF to 1, else to 0
    ShiftLeft(u8),

    /// `9XY0`
    ///
    /// Skips the next instruction if the value in VX is not equal to the value in VY
    SkipVNotEquals(u8, u8),

    /// `ANNN`
    ///
    /// Set the index register to NNN
    SetIndex(u16),

    /// `BXNN`
    ///
    /// Jump to XNN + VX
    JumpOffset(u16, u8),

    /// `CXNN`
    ///
    /// Generate a random number, binary and it with NN and save it to VX
    Random(u8, u8),

    /// `DXYN`
    ///
    /// Draws a sprite at the location of the index register to the X coordinate in VX and Y
    /// coordinate in VY with the height N.
    ///
    /// If N is 0, draw a 16x16 sprite.
    ///
    /// If any pixels have been turned off, VF is set to 1, else to 0.
    Draw(u8, u8, u8),

    /// `EX9E`
    ///
    /// Skip the next instruction if the key corresponding to the value of VX is held down
    SkipIfKey(u8),

    /// `EXA1`
    ///
    /// Skip the next instruction if the key corresponding to the value of VX is not held down
    SkipIfNotKey(u8),

    /// `FX07`
    ///
    /// Sets VX to the current value of the delay timer
    GetDelay(u8),

    /// `FX0A`
    ///
    /// Blocks until a key is pressed (and released)
    /// Puts the number of the pressed key into VX
    GetKey(u8),

    /// `FX15`
    ///
    /// Set the delay timer to VX
    SetDelay(u8),

    /// `FX18`
    ///
    /// Set the sound timer to VX
    SetSound(u8),

    /// `FX1E`
    ///
    /// Set index to index + VX
    AddToIndex(u8),

    /// `FX29`
    ///
    /// Set index to the address of the font corresponding to VX
    Font(u8),

    /// `FX30`
    ///
    /// Set index to the address of the large font corresponding to VX
    LargeFont(u8),

    /// `FX33`
    ///
    /// Split the digits of VX and save them into memory starting at the index
    BinaryDecimalConversion(u8),

    /// `FX55`
    ///
    /// Save V0-VX to memory starting at index
    Store(u8),

    /// `FX65`
    ///
    /// Load V0-VX from memory starting at index
    Load(u8),

    /// `FX75`
    ///
    /// Store V0-VX to disk
    SaveFlags(u8),

    /// `FX75`
    ///
    /// Load V0-VX from disk
    LoadFlags(u8),
}

impl TryFrom<u16> for Opcode {
    type Error = Error;

    /// Try to construct an Opcode from a value
    ///
    /// # Errors
    ///
    /// If there is no matching opcode, return an [`Error::UnknownOpcode`]
    fn try_from(value: u16) -> Result<Self, Self::Error> {
        log::trace!("{value:02X}");
        let w: u8 = (value >> 12).try_into().expect("Shozkd always fit an u8");
        let x: u8 = ((value & 0x0F00) >> 8)
            .try_into()
            .expect("Should always fit an u8");
        let y: u8 = ((value & 0x00F0) >> 4)
            .try_into()
            .expect("Should always fit an u8");
        let xy: u8 = ((value & 0x0FF0) >> 4)
            .try_into()
            .expect("Should always fit an u8");
        let nnn = value & 0x0FFF;
        let nn: u8 = (value & 0x00FF)
            .try_into()
            .expect("Should always fit an u8");
        let n: u8 = (value & 0x000F)
            .try_into()
            .expect("Should always fit an u8");
        match w {
            0x0 => match xy {
                0x0C => Ok(Self::ScrollDown(n)),
                0x0E => match n {
                    0x0 => Ok(Self::ClearScreen),
                    0xE => Ok(Self::Return),
                    _ => Err(Error::UnknownOpcode(value)),
                },
                0x0F => match n {
                    0xB => Ok(Self::ScrollRight),
                    0xC => Ok(Self::ScrollLeft),
                    0xD => Ok(Self::Exit),
                    0xE => Ok(Self::LowRes),
                    0xF => Ok(Self::HighRes),
                    _ => Err(Error::UnknownOpcode(value)),
                },
                _ => Err(Error::UnknownOpcode(value)),
            },
            0x1 => Ok(Self::Jump(nnn)),
            0x2 => Ok(Self::Call(nnn)),
            0x3 => Ok(Self::SkipEquals(x, nn)),
            0x4 => Ok(Self::SkipNotEquals(x, nn)),
            0x5 => {
                if n != 0 {
                    Err(Error::UnknownOpcode(value))
                } else {
                    Ok(Self::SkipVEquals(x, y))
                }
            }
            0x6 => Ok(Self::SetV(x, nn)),
            0x7 => Ok(Self::AddValue(x, nn)),
            0x8 => match n {
                0x0 => Ok(Self::SetVtoV(x, y)),
                0x1 => Ok(Self::Or(x, y)),
                0x2 => Ok(Self::And(x, y)),
                0x3 => Ok(Self::Xor(x, y)),
                0x4 => Ok(Self::AddRegister(x, y)),
                0x5 => Ok(Self::SubtractX(x, y)),
                0x6 => Ok(Self::ShiftRight(x)),
                0x7 => Ok(Self::SubtractY(x, y)),
                0xE => Ok(Self::ShiftLeft(x)),
                _ => Err(Error::UnknownOpcode(value)),
            },
            0x9 => {
                if n != 0 {
                    Err(Error::UnknownOpcode(value))
                } else {
                    Ok(Self::SkipVNotEquals(x, y))
                }
            }
            0xA => Ok(Self::SetIndex(nnn)),
            0xB => Ok(Self::JumpOffset(nnn, x)),
            0xD => Ok(Self::Draw(x, y, n)),
            0xC => Ok(Self::Random(x, nn)),
            0xE => match nn {
                0x9E => Ok(Self::SkipIfKey(x)),
                0xA1 => Ok(Self::SkipIfNotKey(x)),
                _ => Err(Error::UnknownOpcode(value)),
            },
            0xF => match nn {
                0x07 => Ok(Self::GetDelay(x)),
                0x0A => Ok(Self::GetKey(x)),
                0x15 => Ok(Self::SetDelay(x)),
                0x18 => Ok(Self::SetSound(x)),
                0x1E => Ok(Self::AddToIndex(x)),
                0x29 => Ok(Self::Font(x)),
                0x30 => Ok(Self::LargeFont(x)),
                0x33 => Ok(Self::BinaryDecimalConversion(x)),
                0x55 => Ok(Self::Store(x)),
                0x65 => Ok(Self::Load(x)),
                0x75 => Ok(Self::SaveFlags(x)),
                0x85 => Ok(Self::LoadFlags(x)),
                _ => Err(Error::UnknownOpcode(value)),
            },
            _ => Err(Error::UnknownOpcode(value)),
        }
    }
}

impl Opcode {
    /// Executes this Opcode
    ///
    /// # Errors
    ///
    /// If the execution fails, return a [`Error::FailedExecution`]
    pub(crate) fn execute(self, chip8: &mut Chip8) -> Result<bool, Error> {
        log::debug!("Executing: {self:?}");

        match self {
            // Scrolls down by N lines
            Opcode::ScrollDown(n) => chip8.display.scroll_down(n as usize),

            // Scrolls down by N lines
            Opcode::ClearScreen => chip8.display.clear(),

            // Return from subroutine
            Opcode::Return => {
                let return_address = chip8
                    .stack
                    .pop()
                    .map_err(|err| Error::FailedExecution(self, err.into()))?;
                log::trace!("Returning to address {return_address:02X}");
                chip8.registers.pc = return_address;
            }

            // Scrolls right by 4 columns
            Opcode::ScrollRight => chip8.display.scroll_right(),

            // Scrolls left by 4 columns
            Opcode::ScrollLeft => chip8.display.scroll_left(),

            // Exits the current running program
            Opcode::Exit => return Ok(true),

            // Switch screen to Low Res mode
            Opcode::LowRes => chip8.display = Display::new_low_res(),

            //Switch screen to High Res mode
            Opcode::HighRes => chip8.display = Display::new_high_res(),

            // Jumps to the address NNN
            Opcode::Jump(address) => chip8.registers.pc = address,

            // Pushes the current pc to the stack and jumps to NNN
            Opcode::Call(address) => {
                log::trace!("Pushing {:02X} onto the stack", chip8.registers.pc);
                chip8
                    .stack
                    .push(chip8.registers.pc)
                    .map_err(|err| Error::FailedExecution(self, err.into()))?;
                chip8.registers.pc = address;
            }

            // Skips the next instruction if the value in VX is equal to NN
            Opcode::SkipEquals(x, nn) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let comparison = x == nn;
                trace!("{} == {} := {}", x, nn, comparison);
                if comparison {
                    log::trace!("Skipping");
                    chip8.registers.pc += 2;
                }
            }

            // Skips the next instruction if the value in VX is not equal to NN
            Opcode::SkipNotEquals(x, nn) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let comparison = x != nn;
                trace!("{} != {} := {}", x, nn, comparison);
                if comparison {
                    log::trace!("Skipping");
                    chip8.registers.pc += 2;
                }
            }

            // Skips the next instruction if the value in VX is equal to the value in VY
            Opcode::SkipVEquals(x, y) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let comparison = x == y;
                trace!("{} == {} := {}", x, y, comparison);
                if comparison {
                    log::trace!("Skipping");
                    chip8.registers.pc += 2;
                }
            }

            // Sets the value in VX to NN
            Opcode::SetV(x, nn) => {
                log::trace!("Setting V{x:X} to {nn}");
                *chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))? = nn;
            }

            // Adds NN to VX without putting the Overflow in VF
            Opcode::AddValue(x, nn) => {
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let result = x.wrapping_add(nn);
                log::trace!("{x} + {nn} := {result}");
                *x = result;
            }

            // Set the value of VX to the value of VY
            Opcode::SetVtoV(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Settinx V{x:X} to {y}");
                *chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))? = y;
            }

            // Set VX to the binary or of VX and VY
            Opcode::Or(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let result = *x | y;
                log::trace!("{x:08b} | {y:08b} := {result:08b}");
                *x = result;
            }

            // Set VX to the binary and of VX and VY
            Opcode::And(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let result = *x & y;
                log::trace!("{x:08b} & {y:08b} := {result:08b}");
                *x = result;
            }

            // Set VX to the binary xor of VX and VY
            Opcode::Xor(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let result = *x ^ y;
                log::trace!("{x:08b} ^ {y:08b} := {result:08b}");
                *x = result;
            }

            // Set VX to VX plus VY
            // If an overflow occurs, set VF to 1, else to 0
            Opcode::AddRegister(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let (result, overflow) = x.overflowing_add(y);
                trace!("{x} + {y} = {result} VF: {}", u8::from(overflow));
                *x = result;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = overflow.into();
            }

            // Set VX to VX minus VY
            // If an underflow occurs, set VF to 0, else to 1
            Opcode::SubtractX(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let (result, overflow) = x.overflowing_sub(y);
                trace!("{x} + {y} = {result} VF: {}", u8::from(!overflow));
                *x = result;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = (!overflow).into();
            }

            // Shift VX one bit to the right.
            // If an underflow occurs, set VF to 1, else to 0
            Opcode::ShiftRight(x) => {
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let flag = *x & 1;
                let result = *x >> 1;
                log::trace!("{x:08b} >> 1 := {result:08b} VF: {flag}");
                *x = result;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = flag;
            }

            // Set VX to VY minus VX
            // If an underflow occurs, set VF to 0, else to 1
            Opcode::SubtractY(x, y) => {
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let (result, overflow) = y.overflowing_sub(*x);
                trace!("{y} + {x} = {result} VF: {}", u8::from(!overflow));
                *x = result;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = (!overflow).into();
            }

            // Shift VX one bit to the left.
            // If an overflow occurs, set VF to 1, else to 0
            Opcode::ShiftLeft(x) => {
                let v_reg_length = chip8.registers.v.len();
                let x = chip8
                    .registers
                    .v
                    .get_mut(usize::from(x))
                    .ok_or(Error::FailedExecution(
                        self,
                        ExecutionError::OutOfBounds {
                            structure: String::from("Registers"),
                            length: v_reg_length,
                            index: x.into(),
                        },
                    ))?;
                let flag = *x >> 7;
                let result = *x << 1;
                log::trace!("{x:08b} << 1 := {result:08b} VF: {flag}");
                *x = result;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = flag;
            }

            // Skips the next instruction if the value in VX is not equal to the value in VY
            Opcode::SkipVNotEquals(x, y) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let comparison = x != y;
                trace!("{} != {} := {}", x, y, comparison);
                if comparison {
                    log::trace!("Skipping");
                    chip8.registers.pc += 2;
                }
            }

            // Set the index register to NNN
            Opcode::SetIndex(nn) => {
                log::trace!("Set index to {nn}");
                chip8.registers.index = nn;
            }

            // Jump to XNN + VX
            Opcode::JumpOffset(nnn, x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let address = nnn + u16::from(x);
                log::trace!("Jump to {nnn:X} + {x:X} := {address:X}");
                chip8.registers.pc = address;
            }

            // Generate a random number, binary and it with NN and save it to VX
            Opcode::Random(x, nn) => {
                let random = random::<u8>();
                let value = random & nn;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Random {random} & {nn} := {value}");
                *x = value;
            }

            // Draws a sprite at the location of the index register to the X coordinate in VX and Y
            // coordinate in VY with the height N.
            //
            // If N is 0, draw a 16x16 sprite.
            //
            // If any pixels have been turned off, VF is set to 1, else to 0.
            Opcode::Draw(x, y, height) => {
                // draw a single byte
                fn draw_byte(
                    chip8: &mut Chip8,
                    byte: u8,
                    x: usize,
                    y: usize,
                    max_width: usize,
                ) -> Result<(), ExecutionError> {
                    byte.view_bits::<Msb0>()
                        .into_iter()
                        .enumerate()
                        .take_while(|(column, _)| (x + *column) < max_width)
                        .try_for_each(|(column, bit)| {
                            if *bit {
                                let turned_off = chip8.display.set(x + column, y)?;
                                if turned_off {
                                    *chip8
                                        .registers
                                        .v
                                        .get_mut(0xF)
                                        .expect("0xF should always exist") = 1;
                                }
                            };
                            Ok(())
                        })
                }

                let max_width = chip8
                    .display
                    .get(0)
                    .expect("There should always be at least one row")
                    .len();
                let max_height = u8::try_from(chip8.display.len())
                    .expect("The display length always fits into an u8");
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let y = chip8
                    .registers
                    .get_v(usize::from(y))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let x = x % u8::try_from(max_width).expect("Display width always fits in an u8");
                let y = y % max_height;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = 0;

                if height == 0 {
                    // 16x16 Mode
                    (0..16)
                        .take_while(|row| (y + row) < max_height)
                        .try_for_each(|row| {
                            let index = usize::from(chip8.registers.index + 2 * u16::from(row));
                            draw_byte(
                                chip8,
                                *chip8.memory.get(index).ok_or(ExecutionError::OutOfBounds {
                                    structure: String::from("Memory"),
                                    length: chip8.memory.len(),
                                    index,
                                })?,
                                x as usize,
                                (y + row) as usize,
                                max_width,
                            )?;
                            let index: usize =
                                (chip8.registers.index + 2 * u16::from(row) + 1).into();
                            draw_byte(
                                chip8,
                                *chip8.memory.get(index).ok_or(ExecutionError::OutOfBounds {
                                    structure: String::from("Memory"),
                                    length: chip8.memory.len(),
                                    index,
                                })?,
                                x as usize + 8,
                                (y + row) as usize,
                                max_width,
                            )
                        })
                        .map_err(|err| Error::FailedExecution(self, err))?;
                } else {
                    // Normal mode
                    (0..height)
                        .take_while(|row| (y + row) < max_height)
                        .try_for_each(|row| {
                            let index = usize::from(chip8.registers.index + u16::from(row));
                            draw_byte(
                                chip8,
                                *chip8.memory.get(index).ok_or(ExecutionError::OutOfBounds {
                                    structure: String::from("Memory"),
                                    length: chip8.memory.len(),
                                    index,
                                })?,
                                x as usize,
                                (y + row) as usize,
                                max_width,
                            )
                        })
                        .map_err(|err| Error::FailedExecution(self, err))?;
                }
            }

            // Skip the next instruction if the key corresponding to the value of VX is held down
            Opcode::SkipIfKey(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let key = chip8
                    .keys
                    .get_key(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Key {x:X}: {key}");
                if key {
                    log::trace!("Skipping");
                    chip8.registers.pc += 2;
                }
            }

            // Skip the next instruction if the key corresponding to the value of VX is not held down
            Opcode::SkipIfNotKey(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let key = chip8
                    .keys
                    .get_key(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Key {x:X}: {key}");
                if !key {
                    log::trace!("Skipping");
                    chip8.registers.pc += 2;
                }
            }

            // Sets VX to the current value of the delay timer
            Opcode::GetDelay(x) => {
                let delay_timer = chip8.timers.delay_timer;
                let x = chip8
                    .registers
                    .get_v_mut(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Delay Timer: {delay_timer}");
                *x = delay_timer;
            }

            // Blocks until a key is pressed (and released)
            // Puts the number of the pressed key into VX
            Opcode::GetKey(x) => {
                match chip8
                    .keys
                    .iter()
                    .zip(chip8.lastkeys.iter())
                    .enumerate()
                    .find_map(|(key, (pressed, last_pressed))| {
                        if !*pressed && *last_pressed {
                            Some(key)
                        } else {
                            None
                        }
                    }) {
                    None => {
                        log::trace!("No key pressed");
                        chip8.registers.pc -= 2;
                    }
                    Some(key) => {
                        log::trace!("Setting V{x:X} to {key}");
                        let x = chip8
                            .registers
                            .get_v_mut(usize::from(x))
                            .map_err(|e| Error::FailedExecution(self, e))?;
                        *x = u8::try_from(key).expect("Key always fits in an u8");
                    }
                };
            }

            // Set the delay timer to VX
            Opcode::SetDelay(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Set Delay timer to {x}");
                chip8.timers.delay_timer = x;
            }

            // Set the sound timer to VX
            Opcode::SetSound(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                log::trace!("Set Sound timer to {x}");
                chip8.timers.sound_timer = x;
            }

            // Set index to index + VX
            Opcode::AddToIndex(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let (value, overflow) = chip8.registers.index.overflowing_add(x.into());
                log::trace!(
                    "{} + {x} := {value} Overflow: {overflow}",
                    chip8.registers.index
                );
                chip8.registers.index = value;
                *chip8
                    .registers
                    .v
                    .get_mut(0xF)
                    .expect("0xF should always exist") = overflow.into();
            }

            // Set index to the address of the font corresponding to VX
            Opcode::Font(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let font = u16::from(x) * 5;
                log::trace!("Font {x:X} is at {font:X}");
                chip8.registers.index = font;
            }

            // Set index to the address of the large font corresponding to VX
            Opcode::LargeFont(x) => {
                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let font = u16::from(x) * 10
                    + u16::try_from(memory::SCHIP_FONTS_START)
                        .expect("The whole address range should fit into an u16");
                log::trace!("Large font {x:X} is at {font:X}");
                chip8.registers.index = font;
            }

            // Split the digits of VX and save them into memory starting at the index
            Opcode::BinaryDecimalConversion(x) => {
                let mem_len = chip8.memory.len();

                let x = chip8
                    .registers
                    .get_v(usize::from(x))
                    .map_err(|e| Error::FailedExecution(self, e))?;
                let a = x / 100;
                log::trace!("Save {a} to {:X}", chip8.registers.index);
                *chip8
                    .memory
                    .get_mut(usize::from(chip8.registers.index))
                    .ok_or(Error::FailedExecution(
                        self,
                        ExecutionError::OutOfBounds {
                            structure: String::from("Memory"),
                            length: mem_len,
                            index: chip8.registers.index.into(),
                        },
                    ))? = a;
                let b = (x % 100) / 10;
                log::trace!("Save {b} to {:X}", chip8.registers.index + 1);
                *chip8
                    .memory
                    .get_mut(usize::from(chip8.registers.index + 1))
                    .ok_or(Error::FailedExecution(
                        self,
                        ExecutionError::OutOfBounds {
                            structure: String::from("Memory"),
                            length: mem_len,
                            index: (chip8.registers.index + 1).into(),
                        },
                    ))? = b;
                let c = x % 10;
                log::trace!("Save {c} to {:X}", chip8.registers.index + 2);
                *chip8
                    .memory
                    .get_mut(usize::from(chip8.registers.index + 2))
                    .ok_or(Error::FailedExecution(
                        self,
                        ExecutionError::OutOfBounds {
                            structure: String::from("Memory"),
                            length: mem_len,
                            index: (chip8.registers.index + 2).into(),
                        },
                    ))? = c;
            }

            // Save V0-VX to memory starting at index
            Opcode::Store(x) => {
                let mem_len = chip8.memory.len();
                (0..=x).try_for_each(|x| {
                    let index = usize::from(chip8.registers.index + u16::from(x));
                    let x = chip8
                        .registers
                        .get_v(usize::from(x))
                        .map_err(|e| Error::FailedExecution(self, e))?;
                    log::trace!("Save {x} to {index:X}");
                    *chip8.memory.get_mut(index).ok_or(Error::FailedExecution(
                        self,
                        ExecutionError::OutOfBounds {
                            structure: String::from("Memory"),
                            length: mem_len,
                            index,
                        },
                    ))? = x;
                    Ok(())
                })?;
            }

            // Load V0-VX from memory starting at index
            Opcode::Load(x) => {
                (0..=x).try_for_each(|x| {
                    let index = usize::from(chip8.registers.index + u16::from(x));
                    let value = chip8
                        .memory
                        .get(index)
                        .ok_or(Error::FailedExecution(
                            self,
                            ExecutionError::OutOfBounds {
                                structure: String::from("Memory"),
                                length: chip8.memory.len(),
                                index,
                            },
                        ))
                        .copied()?;
                    log::trace!("Load {value} from {index:X} to {x:X}");
                    let x = chip8
                        .registers
                        .get_v_mut(usize::from(x))
                        .map_err(|e| Error::FailedExecution(self, e))?;
                    *x = value;
                    Ok(())
                })?;
            }

            // Store V0-VX to disk
            Opcode::SaveFlags(x) => {
                let registers =
                    chip8
                        .registers
                        .v
                        .get(0..usize::from(x))
                        .ok_or(Error::FailedExecution(
                            self,
                            ExecutionError::OutOfBounds {
                                structure: String::from("Registers"),
                                length: chip8.registers.v.len(),
                                index: usize::from(x),
                            },
                        ))?;
                log::trace!("Saving {registers:?} to disk");
                (chip8.save_flags)(registers)
                    .map_err(|e| Error::FailedExecution(self, ExecutionError::SaveLoad(e)))?;
            }

            // Load V0-VX from disk
            Opcode::LoadFlags(x) => {
                let reg_len = chip8.registers.v.len();
                let registers =
                    chip8
                        .registers
                        .v
                        .get_mut(0..usize::from(x))
                        .ok_or(Error::FailedExecution(
                            self,
                            ExecutionError::OutOfBounds {
                                structure: String::from("Registers"),
                                length: reg_len,
                                index: usize::from(x),
                            },
                        ))?;
                let mut loaded = (chip8.load_flags)()
                    .map_err(|e| Error::FailedExecution(self, ExecutionError::SaveLoad(e)))?;
                // Pad with 0
                if loaded.len() < registers.len() {
                    loaded.extend(iter::repeat(0).take(registers.len() - loaded.len()));
                }
                let loaded = loaded.get(0..usize::from(x)).ok_or(Error::FailedExecution(
                    self,
                    ExecutionError::OutOfBounds {
                        structure: String::from("Loaded Registers"),
                        length: loaded.len(),
                        index: usize::from(x),
                    },
                ))?;
                log::trace!("Loading {loaded:?} from disk");
                registers.copy_from_slice(loaded);
            }
        }
        Ok(false)
    }
}

/// Anything that can go wrong with Opcodes
#[derive(ThisError, Debug)]
pub enum Error {
    /// Tried to construct an Opcode from a value that isn't a opcode
    #[error("Unknown Opcode: {0:04X}")]
    UnknownOpcode(u16),

    /// Something failed when trying to execute the opcode
    #[error("Couldn't execute {0:?}: {1}")]
    FailedExecution(Opcode, #[source] ExecutionError),
}

/// Errors that can happen at execution
#[derive(ThisError, Debug)]
pub enum ExecutionError {
    /// A Stack operation failed
    #[error("A stack operation failed: {0}")]
    StackOperation(#[from] stack::Error),

    /// Tried to access some array out of bounds
    #[error("Out of bounds read. {structure} has a length of {length}, index was {index}")]
    OutOfBounds {
        structure: String,
        length: usize,
        index: usize,
    },

    /// Some error occured in the save or load callback
    #[error("And error occured while saving or loading flags: {0}")]
    SaveLoad(#[from] Box<dyn std::error::Error + Send + Sync>),
}
