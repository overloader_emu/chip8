use bitvec::{array::BitArray, order::Msb0, slice::BitSlice, BitArr};

use crate::opcodes;

/// The display of the emulator.
///
/// Should be rendered at 60HZ, though that is up to the frontend
///
/// Pixels that are turned on represent a 1 in the bits array
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum Display {
    /// The small grid for low resolution mode
    LowRes(Box<[BitArr!(for 64, in u64, Msb0); 32]>),
    /// The large grid for high resolution mode
    HighRes(Box<[BitArr!(for 128, in u64, Msb0); 64]>),
}

impl Default for Display {
    fn default() -> Self {
        Self::new_low_res()
    }
}

impl Display {
    pub(crate) fn new_low_res() -> Self {
        Self::LowRes(Box::new([BitArray::ZERO; 32]))
    }

    pub(crate) fn new_high_res() -> Self {
        Self::HighRes(Box::new([BitArray::ZERO; 64]))
    }

    /// Sets a pixel. It gets XOR'd with what is already on the screen.
    ///
    /// If this turns a pixel off, return true
    ///
    /// # Errors
    ///
    /// If trying to set a pixel out of bounds return a [`opcodes::ExecutionError::OutOfBounds`]
    pub(crate) fn set(&mut self, x: usize, y: usize) -> Result<bool, opcodes::ExecutionError> {
        let self_len = self.len();
        let row = self
            .get_mut(y)
            .ok_or(opcodes::ExecutionError::OutOfBounds {
                structure: String::from("Display"),
                length: self_len,
                index: y,
            })
            .inspect_err(|e| log::error!("{e}"))?;
        let row_len = row.len();
        let cell = row
            .get_mut(x)
            .ok_or(opcodes::ExecutionError::OutOfBounds {
                structure: String::from("Display row"),
                length: row_len,
                index: x,
            })
            .inspect_err(|e| log::error!("{e}"))?;
        let previous = *cell;

        log::trace!("Setting ({x},{y}) Previous: {previous}");
        cell.commit(!previous);
        Ok(previous)
    }

    pub fn get(&self, index: usize) -> Option<&BitSlice<u64, Msb0>> {
        match self {
            Display::LowRes(inner) => inner.get(index).map(BitArray::as_bitslice),
            Display::HighRes(inner) => inner.get(index).map(BitArray::as_bitslice),
        }
    }

    fn get_mut(&mut self, index: usize) -> Option<&mut BitSlice<u64, Msb0>> {
        match self {
            Display::LowRes(inner) => inner.get_mut(index).map(BitArray::as_mut_bitslice),
            Display::HighRes(inner) => inner.get_mut(index).map(BitArray::as_mut_bitslice),
        }
    }

    pub(crate) fn clear(&mut self) {
        match self {
            Display::LowRes(_) => *self = Self::new_low_res(),
            Display::HighRes(_) => *self = Self::new_high_res(),
        }
    }

    /// The length of the outer array of the display
    #[allow(clippy::len_without_is_empty)] // Display is never empty
    #[must_use]
    pub fn len(&self) -> usize {
        match self {
            Display::LowRes(inner) => inner.len(),
            Display::HighRes(inner) => inner.len(),
        }
    }

    /// Scrolls the display down a certain amount of rows
    pub(crate) fn scroll_down(&mut self, amount: usize) {
        match self {
            Display::LowRes(inner) => {
                inner.rotate_right(amount);
                // Zero the top
                for element in inner.iter_mut().take(amount) {
                    *element = BitArray::ZERO;
                }
            }
            Display::HighRes(inner) => {
                inner.rotate_right(amount);
                // Zero the top
                for element in inner.iter_mut().take(amount) {
                    *element = BitArray::ZERO;
                }
            }
        }
    }

    /// Scrolls the display left a certain amount of columns
    pub(crate) fn scroll_left(&mut self) {
        match self {
            Display::LowRes(inner) => {
                inner.iter_mut().for_each(|row| {
                    row.rotate_left(4);
                    // Zero the right
                    row.iter_mut()
                        .rev()
                        .take(4)
                        .for_each(|value| value.commit(false));
                });
            }
            Display::HighRes(inner) => {
                inner.iter_mut().for_each(|row| {
                    row.rotate_left(4);
                    // Zero the right
                    row.iter_mut()
                        .rev()
                        .take(4)
                        .for_each(|value| value.commit(false));
                });
            }
        }
    }

    /// Scrolls the display right a certain amount of columns
    pub(crate) fn scroll_right(&mut self) {
        match self {
            Display::LowRes(inner) => {
                inner.iter_mut().for_each(|row| {
                    row.rotate_right(4);
                    // Zero the left
                    row.iter_mut().take(4).for_each(|value| value.commit(false));
                });
            }
            Display::HighRes(inner) => {
                inner.iter_mut().for_each(|row| {
                    row.rotate_right(4);
                    // Zero the left
                    row.iter_mut().take(4).for_each(|value| value.commit(false));
                });
            }
        }
    }
}
