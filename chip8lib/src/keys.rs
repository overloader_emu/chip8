use derive_more::derive::{Deref, DerefMut};

use crate::opcodes;

/// The keys of the emu. Mapping them to hardware keys and setting these is up to the frontend
#[derive(Default, Clone, PartialEq, Eq, Hash, Debug, Deref, DerefMut)]
pub struct Keys([bool; 16]);

impl Keys {
    #[must_use]
    pub fn new(keys: [bool; 16]) -> Self {
        Self(keys)
    }

    /// Get a key
    ///
    /// # Errors
    ///
    /// If the index is too large, return a [`opcodes::ExecutionError::OutOfBounds`]
    pub(crate) fn get_key(&self, index: usize) -> Result<bool, opcodes::ExecutionError> {
        self.get(index)
            .ok_or(opcodes::ExecutionError::OutOfBounds {
                structure: String::from("Keys"),
                length: self.len(),
                index,
            })
            .inspect_err(|e| log::error!("{e}"))
            .copied()
    }

    /// Sets a key
    ///
    /// returns the previous value
    ///
    /// # Errors
    ///
    /// If the index is too large, return [`None`]
    pub fn set_key(&mut self, index: usize, value: bool) -> Option<bool> {
        Some(std::mem::replace(self.get_mut(index)?, value))
    }
}
