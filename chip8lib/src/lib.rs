//! A library acting as a backend for chip8 emulators
//!
//! # Usage
//!
//! First you need to create a [`Chip8`] object
//!
//! To do that you need to create two callbacks:
//! - One [`SaveFlagCallback`]: get's called when flags get saved to the disk
//! - One [`LoadFlagCallback`]: get's called when flags get loaded from the disk
//!
//! They should both use the same location to store to/load from
//!
//! ```
//! // First create the callbacks
//!
//! // The create the Chip8 Object
//! let mut chip8 = Chip8::new(save_flag_callback, load_flag_callback);
//! ```
//!
//! Then you can load a rom
//! ```
//! // Load the rom into some u8 Collection and pass a slice to the function
//! chip8.load_and_start(&rom);
//! ```
//!
//! There are two things you should do at 60HZ: Render the display and decrease the timers
//! ```
//! // Implement something that can render this
//! let display = chip8.display.clone();
//! // Decrement the timers
//! chip8.timers.decrement();
//! ```
//!
//! How fast the Chip8s clock cycle is is up to you but at each cycle you need to call
//! ```
//! chip8.step();
//! ```
//!
//! Lastly: IO
//! While the `sound_timer` is active you should play a sound
//! ```
//! if chip8.timers.sound_timer > 0 {
//!     // Play sound
//! }
//! ```
//!
//! If a key is pressed or released you need to set it accordingly
//! ```
//! // A key has been pressed
//! chip8.keys.set_key(key_number, true).unwrap();
//! ```

use std::{fmt::Debug, mem};

pub use display::Display;
pub use keys::Keys;
use memory::InvalidRom;
pub use memory::Memory;
use opcodes::Opcode;
pub use registers::Registers;
pub use stack::Stack;
pub use timers::Timers;

mod display;
mod keys;
pub mod memory;
pub mod opcodes;
mod registers;
pub mod stack;
mod timers;

/// A callback supposed to save the given bytes to somewhere [`LoadFlagCallback`] can load them
/// from
///
/// # Errors
///
/// Can return any Object that implements [`std::error::Error`] if saving fails
pub type SaveFlagCallback =
    dyn FnMut(&[u8]) -> Result<(), Box<dyn std::error::Error + Send + Sync>>;

/// A callback supposed to load bytes from the same location [`SaveFlagCallback`] saves them too
///
/// # Errors
///
/// Can return any Object that implements [`std::error::Error`] if loading fails
pub type LoadFlagCallback =
    dyn FnMut() -> Result<Vec<u8>, Box<dyn std::error::Error + Send + Sync>>;

/// A chip8 Emulator. Check the crate level docs
pub struct Chip8 {
    memory: Memory,
    registers: Registers,
    display: Display,
    stack: Stack,
    pub timers: Timers,
    /// The currently pressed keys
    keys: Keys,
    /// The lastly pressed keys
    lastkeys: Keys,
    save_flags: Box<SaveFlagCallback>,
    load_flags: Box<LoadFlagCallback>,
}

impl Debug for Chip8 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Chip8")
            .field("memory", &self.memory)
            .field("registers", &self.registers)
            .field("display", &self.display)
            .field("stack", &self.stack)
            .field("timers", &self.timers)
            .field("keys", &self.keys)
            .field("lastkeys", &self.lastkeys)
            .finish_non_exhaustive()
    }
}

impl Chip8 {
    /// Creates a new Emulator with the given callbacks used to load and save flags
    #[must_use]
    pub fn new(save_flags: Box<SaveFlagCallback>, load_flags: Box<LoadFlagCallback>) -> Self {
        Self {
            memory: Memory::default(),
            registers: Registers::default(),
            display: Display::default(),
            stack: Stack::default(),
            timers: Timers::default(),
            keys: Keys::default(),
            lastkeys: Keys::default(),
            save_flags,
            load_flags,
        }
    }

    #[must_use]
    pub fn display(&self) -> &Display {
        &self.display
    }

    #[must_use]
    pub fn stack(&self) -> &Stack {
        &self.stack
    }

    #[must_use]
    pub fn memory(&self) -> &Memory {
        &self.memory
    }

    #[must_use]
    pub fn registers(&self) -> &Registers {
        &self.registers
    }

    /// Load a rom and set internal state to start conditions
    ///
    /// # Errors
    ///
    /// [`InvalidRom`] if the given rom is too large
    pub fn load_and_start(&mut self, rom: &[u8]) -> Result<(), InvalidRom> {
        self.registers.clear();
        self.display = Display::default();
        self.stack = Stack::default();
        self.timers = Timers::default();

        self.keys = Keys::default();
        self.lastkeys = Keys::default();
        self.memory = Memory::default();
        self.memory.load_rom(rom)?;
        self.registers.pc = 0x200;
        Ok(())
    }

    /// Register which keys have been pressed/released since the last time calling this
    pub fn new_keys(&mut self, keys: [bool; 16]) {
        self.lastkeys = mem::replace(&mut self.keys, Keys::new(keys));
    }

    /// Do a single execution step
    ///
    /// # Errors
    ///
    /// [`opcodes::Error`] if something went wrong during execution
    #[allow(clippy::missing_panics_doc)] // These panics can't happen
    pub fn step(&mut self) -> Result<bool, opcodes::Error> {
        let instruction = u16::from_be_bytes(
            self.memory
                .get(usize::from(self.registers.pc)..=usize::from(self.registers.pc + 1))
                .expect("The PC can never go oob")
                .try_into()
                .expect("Has always a length of 2"),
        );
        self.registers.pc += 2;
        let instruction = Opcode::try_from(instruction)?;
        instruction.execute(self)
    }
}
