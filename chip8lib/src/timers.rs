/// The inbuilt timers.
///
/// While `sound_timer` > 0 you need to play sound
#[derive(Default, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Timers {
    pub delay_timer: u8,
    pub sound_timer: u8,
}

impl Timers {
    /// Decrement the timers by one.
    ///
    /// This should be called at 60HZ
    pub fn decrement(&mut self) {
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
            log::debug!("Decrementing delay timer to {}", self.delay_timer);
        }
        if self.sound_timer > 0 {
            self.sound_timer -= 1;
            log::debug!("Decrementing sound timer to {}", self.sound_timer);
        }
    }
}
