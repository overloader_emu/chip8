A library acting as a backend for chip8 emulators

# Usage

First you need to create a [`Chip8`] object

To do that you need to create two callbacks:
- One [`SaveFlagCallback`]: get's called when flags get saved to the disk
- One [`LoadFlagCallback`]: get's called when flags get loaded from the disk

They should both use the same location to store to/load from

```rust
// First create the callbacks

// The create the Chip8 Object
let mut chip8 = Chip8::new(save_flag_callback, load_flag_callback);
```

Then you can load a rom
```rust
// Load the rom into some u8 Collection and pass a slice to the function
chip8.load_and_start(&rom);
```

There are two things you should do at 60HZ: Render the display and decrease the timers
```rust
// Implement something that can render this
let display = chip8.display.clone();
// Decrement the timers
chip8.timers.decrement();
```

How fast the Chip8s clock cycle is is up to you but at each cycle you need to call
```rust
chip8.step();
```

Lastly: IO
While the `sound_timer` is active you should play a sound
```rust
if chip8.timers.sound_timer > 0 {
    // Play sound
}
```

If a key is pressed or released you need to set it accordingly
```rust
// A key has been pressed
chip8.keys.set_key(key_number, true).unwrap();
```
