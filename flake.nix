{
  description = "A Super Chip8 Emulator written in rust";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { nixpkgs, rust-overlay, ... }:
    let
      system = "x86_64-linux";
      overlays = [ (import rust-overlay) ];
      pkgs = import nixpkgs {
        inherit system overlays;
      };
      libraries = with pkgs; [
        wayland
        libxkbcommon
        libGL
      ];
      inputs = with pkgs; [
        glib
        gdk-pixbuf
        pango
        at-spi2-atk
        gtk3
        glib
        alsa-lib
      ];
      nativeInputs = with pkgs; [
        pkg-config
      ];

    in
    {
      devShells."${system}".default =
        pkgs.mkShell.override { stdenv = pkgs.stdenvAdapters.useMoldLinker pkgs.clangStdenv; }
          {
            packages =
              with pkgs;
              [
                rust-bin.stable.latest.default
                cargo-deny
                cargo-machete
                cargo-audit
              ]
              ++ inputs
              ++ nativeInputs;
            GSETTINGS_SCHEMA_DIR = "${pkgs.gtk3}/share/gsettings-schemas/gtk+3-3.24.43/glib-2.0/schemas";
            NIX_LDFLAGS = pkgs.lib.concatStringsSep " " (map (value: "-rpath ${value}/lib") libraries);
          };
      packages."${system}" = rec {
        default = ruschip;
        ruschip = pkgs.rustPlatform.buildRustPackage rec {
          pname = "ruschip";
          version = "1.0.0";

          nativeBuildInputs = nativeInputs;
          buildInputs = inputs;

          cargoLock.lockFile = ./Cargo.lock;
          src = pkgs.lib.cleanSource ./.;

          GSETTINGS_SCHEMA_DIR = "${pkgs.gtk3}/share/gsettings-schemas/gtk+3-3.24.43/glib-2.0/schemas";
          postFixup = ''
            patchelf $out/bin/${pname} \
              --add-rpath ${pkgs.lib.makeLibraryPath libraries}
          '';

          meta = with pkgs.lib; {
            description = "A Super Chip8 Emulator written in rust";
            license = licenses.bsd3;
          };
        };
      };
    };
}
